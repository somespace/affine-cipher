﻿using MMLib.Extensions;
using System;
using System.Linq;
using System.Text;

namespace AffineCipher
{
    /// <summary>
    /// Monoalfabetická substituční šifra - logická časť
    /// </summary>
    public static class Cipher
    {
        static int a = 0;
        static int b = 0;

        private static string[] alphabet =
        { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K",
            "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

        public static int alphabetLen = alphabet.Length;

        public static string[] nonAlphabetChars =
        {
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
            ".", ",", ":", ";", "?", "!", "\"", "\'", "\\", "/",
            "~", "@", "#", "%", "^", "&", "*", "(", ")", "_", "-",
            "+", "=", "[", "]", "{", "}", " "
        };

        public static string[] nonAlphabetCharCodes =
        {
            "IWPTZBHRGH", "YZQELPWQVI", "VWEFGPZYHN", "IOQQTRZBTO", "QYVZXLLPLB",
            "PXVZCVNHPW", "POIWPUFMRI", "XVMCZXCPUI", "RWTRWBGNPO", "XNMWVYHLPW",
            "QGEWLHWRRN", "OVTXRIOENI", "HRHKFRIHWS", "JOGNFDXSXC", "NRDOUTEWFJ",
            "OWCBGEQSTO", "POWEBRQZXC", "HQZPTRCKVX", "NIOWBEOROS", "OWENRZCKVD",
            "CSSJERBECV", "WIOERNXWVZ", "WOEJRNWERO", "XCVCIZXNES", "WIOENZCVZQ",
            "ZYXNMCXOWI", "IOWERNQQOI", "OPOWPQYWEX", "ZDTLONWQOW", "PXCVWQSLKW",
            "ZOIVWNOQPP", "MOUEIYMMEI", "ZNCVBWBVEW", "CIEWOIPBMH", "NESDDSVRBH",
            "OWFGFGONCV", "MQEIRQNNIS", "XMEZERAX"
        };

        public static void AssignValueToKey(int aValue, int bValue)
        {
            a = aValue;
            b = bValue;
        }
        
        public static string FilterText(string text)
        {
            string diaremoved = text.RemoveDiacritics();
            string filtered = "";

            for(int i = 0; i < diaremoved.Length; i++)
            {
                if (alphabet.Contains(diaremoved[i].ToString().ToUpper())) 
                {
                    filtered += diaremoved[i].ToString().ToUpper();
                }
                else if (nonAlphabetChars.Contains(diaremoved[i].ToString()))
                {
                    int specialCharPosition = Array.IndexOf(nonAlphabetChars, diaremoved[i].ToString());
                    filtered += nonAlphabetCharCodes[specialCharPosition];
                }
            }

            return filtered;
        }

        private static string DefilterText(string filtered)
        {
            StringBuilder defilteredText = new StringBuilder(filtered);

            for (int i = 0; i < nonAlphabetCharCodes.Length; i++)
            {
                if (filtered.Contains(nonAlphabetCharCodes[i]))
                {
                    defilteredText.Replace(nonAlphabetCharCodes[i], nonAlphabetChars[i]);
                }
            }
            return defilteredText.ToString();
        }

        public static string EncryptText(string filtered)
        {
            string encryptedText = "";

            for (int i = 0; i < filtered.Length; i++)
            {
                string filteredChar = filtered[i].ToString();
                int charPosition = Array.IndexOf(alphabet, filteredChar);
                int encryptedNum = (a * charPosition + b) % alphabet.Length;

                encryptedText += alphabet[encryptedNum];
            }
            return encryptedText;
        }

        public static string FormatOutput(string text)
        {
            string formatted = "";
            for (int i = 0; i < text.Length; i++)
            {
                formatted += text[i];
                if ((i + 1) % 5 == 0)
                {
                    formatted += " ";
                }
            }
            return formatted;
        }

        public static string DecryptText(string encryptedText)
        {
            string decryptedText = "";

            for (int i = 0; i < encryptedText.Length; i++)
            {
                string encryptedChar = encryptedText[i].ToString();
                int encryptedCharPosition = Array.IndexOf(alphabet, encryptedChar);
                int decryptedNum =
                    (encryptedCharPosition - b) * MMI(a, alphabet.Length) % alphabet.Length;
                int correctedNum = decryptedNum < 0 ? decryptedNum + alphabet.Length : decryptedNum;

                decryptedText += alphabet[correctedNum];
            }

            return DefilterText(decryptedText);
        }

        public static int GCD(int a, int b)
        {
            int abs_a = Math.Abs(a);
            int abs_b = Math.Abs(b);

            while (a != 0 && b != 0)
            {
                if (a > b)
                    a %= b;
                else
                    b %= a;
            }

            if (a == 0)
                return b;
            else
                return a;
        }

        private static int MMI(int a, int alphabetLength)
        {
            for (int i = 1; i < alphabetLength; i++)
            {
                int calculation = (a * i) % alphabetLength;

                if (calculation == 1)
                {
                    return i;
                }
            }
            return 0;
        }
    }
}
