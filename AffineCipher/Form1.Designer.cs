﻿namespace AffineCipher
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.encryptedAlphabetTextBox = new System.Windows.Forms.TextBox();
            this.encryptedAlphabetLabel = new System.Windows.Forms.Label();
            this.alphabetTextBox = new System.Windows.Forms.TextBox();
            this.alphabetLabel = new System.Windows.Forms.Label();
            this.keyBLabel = new System.Windows.Forms.Label();
            this.keyALabel = new System.Windows.Forms.Label();
            this.keyAnumUpdown = new System.Windows.Forms.NumericUpDown();
            this.keyBnumUpDown = new System.Windows.Forms.NumericUpDown();
            this.ParametersPanel = new System.Windows.Forms.Panel();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.filteredTextLabel = new System.Windows.Forms.Label();
            this.filteredTextRTBox = new System.Windows.Forms.RichTextBox();
            this.encryptedTextRTBox = new System.Windows.Forms.RichTextBox();
            this.originalTextRTBox = new System.Windows.Forms.RichTextBox();
            this.copyButton = new System.Windows.Forms.Button();
            this.encryptButton = new System.Windows.Forms.Button();
            this.encryptedTextLabel = new System.Windows.Forms.Label();
            this.originalTextLabel = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.insertButton = new System.Windows.Forms.Button();
            this.decryptButton = new System.Windows.Forms.Button();
            this.decryptedTextLabel = new System.Windows.Forms.Label();
            this.textToDecryptLabel = new System.Windows.Forms.Label();
            this.decryptedTextRTBox = new System.Windows.Forms.RichTextBox();
            this.textToDecryptRTBox = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.MainTabControl = new System.Windows.Forms.TabControl();
            ((System.ComponentModel.ISupportInitialize)(this.keyAnumUpdown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.keyBnumUpDown)).BeginInit();
            this.ParametersPanel.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.MainTabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // encryptedAlphabetTextBox
            // 
            this.encryptedAlphabetTextBox.Enabled = false;
            this.encryptedAlphabetTextBox.Location = new System.Drawing.Point(535, 99);
            this.encryptedAlphabetTextBox.Name = "encryptedAlphabetTextBox";
            this.encryptedAlphabetTextBox.Size = new System.Drawing.Size(456, 26);
            this.encryptedAlphabetTextBox.TabIndex = 15;
            // 
            // encryptedAlphabetLabel
            // 
            this.encryptedAlphabetLabel.AutoSize = true;
            this.encryptedAlphabetLabel.Location = new System.Drawing.Point(531, 76);
            this.encryptedAlphabetLabel.Name = "encryptedAlphabetLabel";
            this.encryptedAlphabetLabel.Size = new System.Drawing.Size(142, 20);
            this.encryptedAlphabetLabel.TabIndex = 14;
            this.encryptedAlphabetLabel.Text = "Šifrovaná abeceda";
            // 
            // alphabetTextBox
            // 
            this.alphabetTextBox.Enabled = false;
            this.alphabetTextBox.Location = new System.Drawing.Point(537, 34);
            this.alphabetTextBox.Name = "alphabetTextBox";
            this.alphabetTextBox.Size = new System.Drawing.Size(454, 26);
            this.alphabetTextBox.TabIndex = 11;
            this.alphabetTextBox.Text = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            // 
            // alphabetLabel
            // 
            this.alphabetLabel.AutoSize = true;
            this.alphabetLabel.Location = new System.Drawing.Point(531, 11);
            this.alphabetLabel.Name = "alphabetLabel";
            this.alphabetLabel.Size = new System.Drawing.Size(73, 20);
            this.alphabetLabel.TabIndex = 10;
            this.alphabetLabel.Text = "Abeceda";
            // 
            // keyBLabel
            // 
            this.keyBLabel.AutoSize = true;
            this.keyBLabel.Location = new System.Drawing.Point(15, 76);
            this.keyBLabel.Name = "keyBLabel";
            this.keyBLabel.Size = new System.Drawing.Size(56, 20);
            this.keyBLabel.TabIndex = 3;
            this.keyBLabel.Text = "Kľúč B";
            // 
            // keyALabel
            // 
            this.keyALabel.AutoSize = true;
            this.keyALabel.Location = new System.Drawing.Point(15, 11);
            this.keyALabel.Name = "keyALabel";
            this.keyALabel.Size = new System.Drawing.Size(56, 20);
            this.keyALabel.TabIndex = 2;
            this.keyALabel.Text = "Kľúč A";
            // 
            // keyAnumUpdown
            // 
            this.keyAnumUpdown.Location = new System.Drawing.Point(19, 34);
            this.keyAnumUpdown.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.keyAnumUpdown.Name = "keyAnumUpdown";
            this.keyAnumUpdown.Size = new System.Drawing.Size(114, 26);
            this.keyAnumUpdown.TabIndex = 1;
            // 
            // keyBnumUpDown
            // 
            this.keyBnumUpDown.Location = new System.Drawing.Point(19, 99);
            this.keyBnumUpDown.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.keyBnumUpDown.Name = "keyBnumUpDown";
            this.keyBnumUpDown.Size = new System.Drawing.Size(114, 26);
            this.keyBnumUpDown.TabIndex = 0;
            // 
            // ParametersPanel
            // 
            this.ParametersPanel.BackColor = System.Drawing.SystemColors.Window;
            this.ParametersPanel.Controls.Add(this.keyALabel);
            this.ParametersPanel.Controls.Add(this.encryptedAlphabetTextBox);
            this.ParametersPanel.Controls.Add(this.keyBnumUpDown);
            this.ParametersPanel.Controls.Add(this.encryptedAlphabetLabel);
            this.ParametersPanel.Controls.Add(this.keyAnumUpdown);
            this.ParametersPanel.Controls.Add(this.keyBLabel);
            this.ParametersPanel.Controls.Add(this.alphabetLabel);
            this.ParametersPanel.Controls.Add(this.alphabetTextBox);
            this.ParametersPanel.Location = new System.Drawing.Point(13, 27);
            this.ParametersPanel.Name = "ParametersPanel";
            this.ParametersPanel.Size = new System.Drawing.Size(1011, 145);
            this.ParametersPanel.TabIndex = 17;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage1.Controls.Add(this.filteredTextLabel);
            this.tabPage1.Controls.Add(this.filteredTextRTBox);
            this.tabPage1.Controls.Add(this.encryptedTextRTBox);
            this.tabPage1.Controls.Add(this.originalTextRTBox);
            this.tabPage1.Controls.Add(this.copyButton);
            this.tabPage1.Controls.Add(this.encryptButton);
            this.tabPage1.Controls.Add(this.encryptedTextLabel);
            this.tabPage1.Controls.Add(this.originalTextLabel);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(1003, 448);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Zašifrovanie";
            // 
            // filteredTextLabel
            // 
            this.filteredTextLabel.AutoSize = true;
            this.filteredTextLabel.Location = new System.Drawing.Point(527, 18);
            this.filteredTextLabel.Name = "filteredTextLabel";
            this.filteredTextLabel.Size = new System.Drawing.Size(119, 20);
            this.filteredTextLabel.TabIndex = 17;
            this.filteredTextLabel.Text = "Vyfiltrovaný text";
            // 
            // filteredTextRTBox
            // 
            this.filteredTextRTBox.Location = new System.Drawing.Point(531, 44);
            this.filteredTextRTBox.Name = "filteredTextRTBox";
            this.filteredTextRTBox.Size = new System.Drawing.Size(456, 131);
            this.filteredTextRTBox.TabIndex = 16;
            this.filteredTextRTBox.Text = "";
            // 
            // encryptedTextRTBox
            // 
            this.encryptedTextRTBox.Location = new System.Drawing.Point(531, 219);
            this.encryptedTextRTBox.Name = "encryptedTextRTBox";
            this.encryptedTextRTBox.Size = new System.Drawing.Size(456, 135);
            this.encryptedTextRTBox.TabIndex = 12;
            this.encryptedTextRTBox.Text = "";
            // 
            // originalTextRTBox
            // 
            this.originalTextRTBox.Location = new System.Drawing.Point(15, 44);
            this.originalTextRTBox.Name = "originalTextRTBox";
            this.originalTextRTBox.Size = new System.Drawing.Size(477, 310);
            this.originalTextRTBox.TabIndex = 5;
            this.originalTextRTBox.Text = "";
            // 
            // copyButton
            // 
            this.copyButton.Location = new System.Drawing.Point(791, 378);
            this.copyButton.Name = "copyButton";
            this.copyButton.Size = new System.Drawing.Size(196, 45);
            this.copyButton.TabIndex = 9;
            this.copyButton.Text = "Skopíruj";
            this.copyButton.UseVisualStyleBackColor = true;
            this.copyButton.Click += new System.EventHandler(this.copyButton_Click);
            // 
            // encryptButton
            // 
            this.encryptButton.Location = new System.Drawing.Point(296, 378);
            this.encryptButton.Name = "encryptButton";
            this.encryptButton.Size = new System.Drawing.Size(196, 45);
            this.encryptButton.TabIndex = 7;
            this.encryptButton.Text = "Zašifruj";
            this.encryptButton.UseVisualStyleBackColor = true;
            this.encryptButton.Click += new System.EventHandler(this.encryptButton_Click);
            // 
            // encryptedTextLabel
            // 
            this.encryptedTextLabel.AutoSize = true;
            this.encryptedTextLabel.Location = new System.Drawing.Point(527, 196);
            this.encryptedTextLabel.Name = "encryptedTextLabel";
            this.encryptedTextLabel.Size = new System.Drawing.Size(120, 20);
            this.encryptedTextLabel.TabIndex = 13;
            this.encryptedTextLabel.Text = "Zašifrovaný text";
            // 
            // originalTextLabel
            // 
            this.originalTextLabel.AutoSize = true;
            this.originalTextLabel.Location = new System.Drawing.Point(11, 18);
            this.originalTextLabel.Name = "originalTextLabel";
            this.originalTextLabel.Size = new System.Drawing.Size(99, 20);
            this.originalTextLabel.TabIndex = 4;
            this.originalTextLabel.Text = "Pôvodný text";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage2.Controls.Add(this.insertButton);
            this.tabPage2.Controls.Add(this.decryptButton);
            this.tabPage2.Controls.Add(this.decryptedTextLabel);
            this.tabPage2.Controls.Add(this.textToDecryptLabel);
            this.tabPage2.Controls.Add(this.decryptedTextRTBox);
            this.tabPage2.Controls.Add(this.textToDecryptRTBox);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(1003, 448);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Dešifrovanie";
            // 
            // insertButton
            // 
            this.insertButton.Location = new System.Drawing.Point(304, 375);
            this.insertButton.Name = "insertButton";
            this.insertButton.Size = new System.Drawing.Size(186, 44);
            this.insertButton.TabIndex = 16;
            this.insertButton.Text = "Vlož";
            this.insertButton.UseVisualStyleBackColor = true;
            this.insertButton.Click += new System.EventHandler(this.insertButton_Click);
            // 
            // decryptButton
            // 
            this.decryptButton.Location = new System.Drawing.Point(786, 375);
            this.decryptButton.Name = "decryptButton";
            this.decryptButton.Size = new System.Drawing.Size(201, 44);
            this.decryptButton.TabIndex = 15;
            this.decryptButton.Text = "Dešifruj";
            this.decryptButton.UseVisualStyleBackColor = true;
            this.decryptButton.Click += new System.EventHandler(this.decryptButton_Click);
            // 
            // decryptedTextLabel
            // 
            this.decryptedTextLabel.AutoSize = true;
            this.decryptedTextLabel.Location = new System.Drawing.Point(527, 20);
            this.decryptedTextLabel.Name = "decryptedTextLabel";
            this.decryptedTextLabel.Size = new System.Drawing.Size(99, 20);
            this.decryptedTextLabel.TabIndex = 14;
            this.decryptedTextLabel.Text = "Pôvodný text";
            // 
            // textToDecryptLabel
            // 
            this.textToDecryptLabel.AutoSize = true;
            this.textToDecryptLabel.Location = new System.Drawing.Point(11, 20);
            this.textToDecryptLabel.Name = "textToDecryptLabel";
            this.textToDecryptLabel.Size = new System.Drawing.Size(120, 20);
            this.textToDecryptLabel.TabIndex = 13;
            this.textToDecryptLabel.Text = "Zašifrovaný text";
            // 
            // decryptedTextRTBox
            // 
            this.decryptedTextRTBox.Location = new System.Drawing.Point(531, 46);
            this.decryptedTextRTBox.Name = "decryptedTextRTBox";
            this.decryptedTextRTBox.Size = new System.Drawing.Size(456, 307);
            this.decryptedTextRTBox.TabIndex = 12;
            this.decryptedTextRTBox.Text = "";
            // 
            // textToDecryptRTBox
            // 
            this.textToDecryptRTBox.Location = new System.Drawing.Point(15, 46);
            this.textToDecryptRTBox.Name = "textToDecryptRTBox";
            this.textToDecryptRTBox.Size = new System.Drawing.Size(475, 307);
            this.textToDecryptRTBox.TabIndex = 10;
            this.textToDecryptRTBox.Text = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(516, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 20);
            this.label5.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 80);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 20);
            this.label6.TabIndex = 9;
            // 
            // MainTabControl
            // 
            this.MainTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MainTabControl.Controls.Add(this.tabPage1);
            this.MainTabControl.Controls.Add(this.tabPage2);
            this.MainTabControl.Location = new System.Drawing.Point(13, 199);
            this.MainTabControl.Name = "MainTabControl";
            this.MainTabControl.SelectedIndex = 0;
            this.MainTabControl.Size = new System.Drawing.Size(1011, 481);
            this.MainTabControl.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1036, 692);
            this.Controls.Add(this.MainTabControl);
            this.Controls.Add(this.ParametersPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Monoalfabetická substitučná šifra";
            ((System.ComponentModel.ISupportInitialize)(this.keyAnumUpdown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.keyBnumUpDown)).EndInit();
            this.ParametersPanel.ResumeLayout(false);
            this.ParametersPanel.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.MainTabControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label keyALabel;
        private System.Windows.Forms.NumericUpDown keyAnumUpdown;
        private System.Windows.Forms.NumericUpDown keyBnumUpDown;
        private System.Windows.Forms.Label keyBLabel;
        private System.Windows.Forms.TextBox alphabetTextBox;
        private System.Windows.Forms.Label alphabetLabel;
        private System.Windows.Forms.TextBox encryptedAlphabetTextBox;
        private System.Windows.Forms.Label encryptedAlphabetLabel;
        private System.Windows.Forms.Panel ParametersPanel;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label filteredTextLabel;
        private System.Windows.Forms.RichTextBox filteredTextRTBox;
        private System.Windows.Forms.RichTextBox encryptedTextRTBox;
        private System.Windows.Forms.RichTextBox originalTextRTBox;
        private System.Windows.Forms.Button copyButton;
        private System.Windows.Forms.Button encryptButton;
        private System.Windows.Forms.Label encryptedTextLabel;
        private System.Windows.Forms.Label originalTextLabel;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button insertButton;
        private System.Windows.Forms.Button decryptButton;
        private System.Windows.Forms.Label decryptedTextLabel;
        private System.Windows.Forms.Label textToDecryptLabel;
        private System.Windows.Forms.RichTextBox decryptedTextRTBox;
        private System.Windows.Forms.RichTextBox textToDecryptRTBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabControl MainTabControl;
    }
}

