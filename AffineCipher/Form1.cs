﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AffineCipher
{
    /// <summary>
    /// Monoalfabetická substitučná šifra - interaktívna a vizuálna časť
    /// </summary>
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private bool KeySuccAssigned()
        {
            if (Cipher.GCD(int.Parse(keyAnumUpdown.Value.ToString()), Cipher.alphabetLen) != 1)
            {
                MessageBox.Show("Kľúč A nespĺňa kritériá výberu, zvoľte prosím iný kľúč.",
                    "Nevyhovujúci kľúč", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            else
            {
                Cipher.AssignValueToKey((int)keyAnumUpdown.Value, (int)keyBnumUpDown.Value);
                return true;
            }
        }

        private void encryptButton_Click(object sender, EventArgs e)
        {
            if (originalTextRTBox.Text.Length == 0)
            {
                MessageBox.Show("Zadajte správu pre zakódovanie.",
                    "Chýbajúca správa", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (KeySuccAssigned())
            {
                string originalText = originalTextRTBox.Text.ToString();
                string filteredText = Cipher.FilterText(originalText);

                encryptedAlphabetTextBox.Text = Cipher.EncryptText(alphabetTextBox.Text);

                string encryptedText = Cipher.EncryptText(filteredText);

                filteredTextRTBox.Text = filteredText;
                encryptedTextRTBox.Text = Cipher.FormatOutput(encryptedText);
            }
        }

        private void copyButton_Click(object sender, EventArgs e)
        {
            encryptedTextRTBox.Focus();
            encryptedTextRTBox.SelectAll();
            encryptedTextRTBox.Copy();
        }
        
        private void insertButton_Click(object sender, EventArgs e)
        {
            textToDecryptRTBox.Clear();
            textToDecryptRTBox.Paste();
        }

        private void decryptButton_Click(object sender, EventArgs e)
        {
            if (textToDecryptRTBox.Text.Length == 0)
            {
                MessageBox.Show("Zadajte správu pre dekódovanie.",
                    "Chýbajúca správa", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (KeySuccAssigned())
            {
                string encryptedText = textToDecryptRTBox.Text.Replace(" ", string.Empty);
                decryptedTextRTBox.Text = Cipher.DecryptText(encryptedText);
            }
        }
    }
}
